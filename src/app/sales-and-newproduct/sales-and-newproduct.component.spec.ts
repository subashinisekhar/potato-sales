import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAndNewproductComponent } from './sales-and-newproduct.component';

describe('SalesAndNewproductComponent', () => {
  let component: SalesAndNewproductComponent;
  let fixture: ComponentFixture<SalesAndNewproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAndNewproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAndNewproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
