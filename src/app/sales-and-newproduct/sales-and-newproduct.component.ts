import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UtilService} from '../services/util.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {NewProduct} from '../models/NewProduct.model';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-sales-and-newproduct',
  templateUrl: './sales-and-newproduct.component.html',
  styleUrls: ['./sales-and-newproduct.component.scss']
})
export class SalesAndNewproductComponent implements OnInit {

  currentTab: string;
  potato_sales_data: {};
  table_cols: any[];
  table_data: [];
  sales: any[];

  newproductForm: FormGroup;
  newProducts$: Subject<NewProduct[]> = new Subject<NewProduct[]>();

  constructor(private router: Router, private utilService: UtilService) {
    this.utilService.readJSON('../../assets/potato-sales.json').subscribe(data => {
      this.potato_sales_data = data;
      this.table_cols = this.potato_sales_data['column'];
      this.table_data = this.potato_sales_data['data'];
      this.addSalesTotalToSalesData();
    });
  }

  ngOnInit(): void {
    this.currentTab = this.router.url.includes('sales') ? 'sales' : 'newproduct';

    this.newproductForm = new FormGroup({
      productname: new FormControl(null, Validators.required),
      productid: new FormControl(null, Validators.required),
      productmanager: new FormControl(null),
      salesstartdate: new FormControl(null, Validators.required),
    });

  }

  /*Sales page code */

  hasSubheaders(col) {
    return !!(col.subHeaders);
  }

  getColspan(col): number {
    const colspan = this.hasSubheaders(col) ? col.subHeaders.length : 1;
    return colspan;
  }

  getSubheaders(col): [] {
    let col_subheaders;
    if (this.hasSubheaders(col)) {
      col_subheaders = col.subHeaders;
    }
    return col_subheaders;
  }

  getCellValue(ColumnORSubHeaderObj, row) {
    const fieldname = ColumnORSubHeaderObj.field;
    return row[fieldname];
  }

  addSalesTotalToSalesData() {
    this.table_cols.forEach(eachCol => {
      if (eachCol.header == 'Total sales') {
        eachCol.field = 'totalSales';
      }
    });
    this.table_data.forEach(eachRow => {
      let sales_total = 0;
      for (const [key, value] of Object.entries(eachRow)) {
        if (key.includes('sales')) {
          sales_total = sales_total + Number(value);
        }
      }
      Object.assign(eachRow, {'totalSales': sales_total});
    });
  }

  /*New Product page code */

  onNewProductSubmit() {
    console.log('submitted form controls are  -- ', this.np);
    this.addToNewProducts(this.np);
    // this.newProducts$.next()
  }

  addToNewProducts(newItem) {
    this.newProducts$.pipe(
      map(NewProductList => {
        NewProductList.push(newItem);
      }));
  }

  resetNewProductForm() {
    this.newproductForm.reset();
  }

  get np() {
    return this.newproductForm.value;
  }
}
