import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UtilService {

  constructor(private http: HttpClient) {}

  public readJSON(jsonfile): Observable<any> {
    return this.http.get(jsonfile);
  }
}
