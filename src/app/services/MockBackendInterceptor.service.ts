import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class MockBackendInterceptor implements HttpInterceptor {
  all_users = [
    {'username': 'Potato Farmer', 'password': 'farmer'},
    {'username': 'Potato Seller', 'password': 'seller'}
  ];

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const {url, method, headers, body} = request;

    if (url.endsWith('/authenticate') && method === 'POST') {
      if (this.authenticate(request.body)) {
        return of(new HttpResponse({status: 200, body: request.body}));
      } else {
        return this.error('username or password is incorrect');
      }
    }
    return next.handle(request);
  }

  authenticate(userinfo): boolean {
    const match = this.all_users.filter(user => {
      return user['username'] === userinfo['username'] && user['password'] === userinfo['password'];
    });
    return (match.length === 1) ? true : false;
  }

  error(msg) {
    return throwError({err: {msg}});
  }

}
