import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthService {
  isUserAuthenticated = false;
  currentUser;

  constructor(private http: HttpClient,
              private router: Router) {
  }

  login(username, password) {
    return this.http.post<any>('http://localhost:8080/authenticate', {'username': username, 'password': password}).subscribe(res => {
        this.isUserAuthenticated = true;
        this.currentUser = res;
        this.router.navigate(['/welcome']);
      },
      error => {
        this.router.navigate(['/error']);
      });
  }

  getUserAuthStatus() {
    return this.isUserAuthenticated;
  }

  logout() {
    this.isUserAuthenticated = false;
  }

}
