import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {ErrorComponent} from './error/error.component';
import {SalesAndNewproductComponent} from './sales-and-newproduct/sales-and-newproduct.component';
import {AuthGuardService} from './services/AuthGuard.service';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {
    path: 'welcome', component: WelcomeComponent, children: [
      {path: '', redirectTo: 'newproduct', pathMatch: 'full'},
      {path: 'sales', component: SalesAndNewproductComponent},
      {path: 'newproduct', component: SalesAndNewproductComponent},
    ],
    canActivate: [AuthGuardService]
  },
  {path: 'error', component: ErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
