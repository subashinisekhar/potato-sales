export interface NewProduct {
  productname: string;
  productid: number;
  productmanager: string;
  salestartdate: Date;
}
